import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { TreeComponent } from './tree/tree.component';
import { TableListComponent } from './table-list/table-list.component';
import { TableDynamicComponent } from './table-dynamic/table-dynamic.component';

const routes: Routes = [
  { path: '', redirectTo: 'list-table', pathMatch: 'full' },
  {
    path: 'list-table', component: TableListComponent
  },
  {
    path: 'dynamic-table', component: TableDynamicComponent
  },
  {
    path: 'tree', component: TreeComponent
  }
];

const COMPONENT = [
  TreeComponent,
  TableListComponent,
  TableDynamicComponent
];

@NgModule({
  declarations: [
    ...COMPONENT
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    CoreModule,
    SharedModule
  ],
  exports: [RouterModule]
})
export class ListModule { }
