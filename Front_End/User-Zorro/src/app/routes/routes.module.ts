import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { PassportComponent } from '../layout/passport/passport.component';
import { LoginComponent } from './passport/login/login.component';
import { DefaultComponent } from '../layout/default/default.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'passport', pathMatch: 'full'
  },
  /** 登陆布局 */
  {
    path: 'passport',
    component: PassportComponent,
    children: [
      {
        path: '', redirectTo: 'login', pathMatch: 'full'
      },
      {
        path: 'login', component: LoginComponent
      }
    ]
  },
  /** 主页布局 */
  {
    path: 'default',
    component: DefaultComponent,
    children: [
      {
        path: '', pathMatch: 'full', redirectTo: 'list'
      },
      {
        path: 'list',
        loadChildren: () => import('./list/list.module').then(m => m.ListModule)
      }
    ]
  }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    SharedModule
  ],
  exports: [RouterModule]
})
export class RoutesModule { }
