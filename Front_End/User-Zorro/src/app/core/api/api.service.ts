import { Injectable, Inject, InjectionToken } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs';
import { Observable } from 'rxjs';

export const API_URL = new InjectionToken<string>('apiUrl');
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
    @Inject(API_URL) public urlPrefix,
  ) { }
  get(url: string, params?: any): Observable<any> {
    // tslint:disable-next-line:no-string-literal
    if (params && params['query']) {
      // tslint:disable-next-line:no-string-literal
      params['query'] = JSON.stringify(params['query']);
    }
    const p = new HttpParams({
      fromObject: params
    });
    return this.http.get(this.urlPrefix + url, {
      params: p,
      withCredentials: true
    });
  }
  post(url: string, body?: any, params?: any): Observable<any> {
    console.log('url', url);
    return this.http.post(this.urlPrefix + url, body, {
      // tslint:disable-next-line:object-literal-shorthand
      params: params,
      withCredentials: true
    });
  }
  // tslint:disable-next-line:jsdoc-format
  /**全域打印 */
  isConsole(...args) {
    // switch (args.length) {
    //   case 1:
    //     console.log(args[0]);
    //     break;
    //   case 2:
    //     console.log(args[0], args[1]);
    // }

  }
}
