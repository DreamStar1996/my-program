import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { PassportComponent } from './passport/passport.component';
import { DefaultComponent } from './default/default.component';
import { HeaderComponent } from './default/header/header.component';
import { SiderComponent } from './default/sider/sider.component';
import { BreadcrumbComponent } from './default/breadcrumb/breadcrumb.component';

const COMPONENT = [
  PassportComponent,
  DefaultComponent,
  HeaderComponent,
  SiderComponent,
  BreadcrumbComponent
];

@NgModule({
  declarations: [
    ...COMPONENT,

  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    ...COMPONENT
  ]
})
export class LayoutModule { }
