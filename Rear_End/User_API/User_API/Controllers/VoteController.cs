﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using User_API.VoteDataModels;

namespace User_API.Controllers
{
    /// <summary>
    /// 测试连接数据库
    /// </summary>
    [ApiController]
    [Route("/user")]
    public class VoteController : ControllerBase
    {

        /// <summary>
        /// 测试拉取请求
        /// </summary>
        /// <returns></returns>
        [HttpGet("/test/get")]
        public OkObjectResult get()
        {
            VoteContext DB = new VoteContext();
            List<VoteData> votelist = DB.VoteData.ToList();
            return Ok(from vote in votelist
                      select new
                      {
                          vote.Id,
                          vote.UserId,
                          vote.UserName
                      });
        }

        /// <summary>
        /// 测试发送请求
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        [HttpPost("/test/post")]
        public JsonResult AAA(int? a, int? b)
        {
            if (a == null || b == null)
                return new JsonResult(
                    new
                    {
                        code = 0,
                        result = "aaaaaaaa"
                    });
            return new JsonResult(
                new
                {
                    code = 2000,
                    result = a + "|" + b
                });
        }
    }
}
