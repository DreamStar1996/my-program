﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User_API.NgxDataModels
{
    public class NgxData
    {
        public int PrdtSubCategoryID { get; set; }
        public int LegalEntity { get; set; }
        public string CostCenter { get; set; }
        public string CostCenterial { get; set; }
        public string GitApplication { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string Amount { get; set; }
        public DateTime ApplicationDate { get; set; }
    }
}
